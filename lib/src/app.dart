
import 'package:flutter/material.dart';
import 'package:holamundo/src/pages/contador_page.dart';
import 'package:holamundo/src/pages/home_page.dart.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Center(
        child: ContadorPage(),
      ),
    );
  }
}



/* return MaterialApp(
      home: Center(
        child: HomePage(),
      )
    );*/